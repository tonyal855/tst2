from flask import request, jsonify
from app import create_access_token, create_refresh_token, session, jwt_required
from model.Models import User, Balance
from app import app
import uuid
import pika
import json

@app.route('/', methods=['GET'])
def index():
    return "Payment"

@app.route('/register', methods=['POST'])
def register():
    first_name = request.form['first_name']
    last_name = request.form['last_name']
    phone_number = request.form['phone_number']
    adress = request.form['adress']
    pin = request.form['pin']
    user_id = str(uuid.uuid4())
    balance_id = str(uuid.uuid4())


    user = User(user_id=user_id, first_name=first_name, last_name=last_name, phone_number=phone_number, adress=adress, pin=pin)
    balance = Balance(balance_id=balance_id, amount_top_up=0, balance_before=0, balance_after=0, user_id=user_id)
    if user.validate:
        try:
            user.save()
            balance.save()
            return jsonify({'status': 'Succes','result': user})
        except:
            return jsonify({'status': 'Failed','message': "Phone Number already registered"}), 409
    else:
        return jsonify({'status': 'data input not match'}), 409

@app.route('/login', methods=['POST'])
def login():
    phone_number = request.form['phone_number']
    pin = request.form['pin']

    user = User.objects(phone_number=phone_number, pin=pin).first()
    if user:
        session["user_id"] = user.user_id
        access_token = create_access_token(identity=user.user_id)
        refresh_token = create_refresh_token(identity=user.user_id)

        return jsonify({'status': 'Succes', 'result': {"access_token": access_token, "refresh_token": refresh_token}})
    else:
        return jsonify({'status': 'Failed', 'message': "Phone number and pin doesn’t match"}), 409


@app.route('/topup', methods=['POST'])
@jwt_required
def top_up():
    amount = request.form['amount']
    balance_id = str(uuid.uuid4())

    getLastData = Balance.objects(user_id=session["user_id"]).order_by('-seq').first()
    balanceBefore = getLastData.balance_after

    balanceAfter = int(amount) + int(balanceBefore)
    balance = Balance(balance_id=balance_id, amount_top_up=amount, balance_before=balanceBefore, balance_after=balanceAfter)
    balance.user_id = session["user_id"]
    balance.save()

    return jsonify({'status': 'Succes', 'result': balance})


@app.route('/pay', methods=['POST'])
@jwt_required
def pay():
    amount = request.form['amount']
    remarks = request.form['remarks']
    balance_id = str(uuid.uuid4())


    getLastData = Balance.objects(user_id=session["user_id"]).order_by('-seq').first()
    balanceBefore = getLastData.balance_after

    balanceAfter = int(balanceBefore) - int(amount)
    if balanceAfter >= 0:

        balance = Balance(balance_id=balance_id, amount_top_up=amount, balance_before=balanceBefore,
                      balance_after=balanceAfter, remarks=remarks)
        balance.user_id = session["user_id"]
        balance.save()

        return jsonify({'status': 'Succes', 'result': balance})
    else:
        return jsonify({'status': 'Failed', 'result': "Balance is not enough"}), 409


@app.route('/transfer', methods=['POST'])
@jwt_required
def transfer():
    target_user = request.form['target_user']
    amount = request.form['amount']
    remarks = request.form['remarks']
    balance_id = str(uuid.uuid4())

    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='payment_transfer')

    getLastData = Balance.objects(user_id=session["user_id"]).order_by('-seq').first()
    balanceBefore = getLastData.balance_after

    balanceAfter = int(balanceBefore) - int(amount)
    if balanceAfter >= 0:

        balance = Balance(balance_id=balance_id, amount_top_up=amount, balance_before=balanceBefore,
                          balance_after=balanceAfter, remarks=remarks)
        balance.user_id = session["user_id"]
        balance.save()

        body= {'user_id': session["user_id"],'target_user' : target_user, 'amount': amount, 'remarks': remarks}
        channel.basic_publish(exchange='', routing_key='payment_transfer', body=json.dumps(body))
        connection.close()

        return jsonify({'status': 'Success', 'result': balance})
    else:
        return jsonify({'status': 'Failed', 'result': "Balance is not enough"}), 409




@app.route('/reports', methods=['GET'])
@jwt_required
def report():
    res = Balance.objects(user_id=session["user_id"]).order_by('-seq').to_json()


    return res


@app.route('/profile', methods=['PUT'])
@jwt_required
def profile():
    first_name = request.form['first_name']
    last_name = request.form['last_name']
    address = request.form['address']


    user = User.objects(user_id=session["user_id"]).update(first_name=first_name, last_name=last_name, adress=address)

    return jsonify({'status': 'Succes', 'result': user})