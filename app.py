from flask import Flask, session
from flask_jwt_extended import JWTManager, create_access_token, create_refresh_token, jwt_required
import os
from db.Db import initialize_db
from datetime import timedelta

app = Flask(__name__)
app.config['MONGODB_SETTINGS'] = {
    'host': 'mongodb://localhost/payment'
}
initialize_db(app)

jwt = JWTManager(app)
JWT_SECRET_KEY = str(os.environ.get("JWT_SECRET"))
app.permanent_session_lifetime = timedelta(minutes=360)

app.config['SECRET_KEY'] = 'NoSecret'
app.config['JWT_SECRET'] = 'RahasiaIsNotSecret'
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = 3600


from controllers.Controllers import *



if __name__ == '__main__':
    app.run(host='localhost', port=9191, debug=True)
