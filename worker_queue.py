import pika, os, sys
import json
from mongoengine import connect, Document, StringField, SequenceField, IntField, DateTimeField
from datetime import datetime
import uuid

mongo = connect(host="mongodb://127.0.0.1:27017/payment")

class Balance(Document):
    balance_id = StringField(required=True, unique=True)
    seq = SequenceField()
    amount_top_up = IntField(required=True)
    balance_before = IntField(required=True)
    balance_after = IntField(required=True)
    remarks = StringField(required=False)
    created_date = DateTimeField(default=datetime.now())
    user_id = StringField(required=True)


def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='payment_transfer')

    def callback(ch, method, properties, body):
        data = json.loads(body)
        user_id = data['user_id']
        target_user = data['target_user']
        amount = data['amount']
        remarks = data['remarks']
        balance_id = str(uuid.uuid4())

        try:
            getLastData = Balance.objects(user_id=target_user).order_by('-seq').first()
            balanceBefore = getLastData.balance_after

            balanceAfter = int(amount) + int(balanceBefore)
            balance = Balance(balance_id=balance_id, amount_top_up=amount, balance_before=balanceBefore,
                              balance_after=balanceAfter ,remarks=remarks)
            balance.user_id = target_user
            balance.save()
        except:
            ## ketika error amount transaksi di cancel
            getLastData = Balance.objects(user_id=user_id).order_by('-seq').first()
            balanceBefore = getLastData.balance_after

            balanceAfter = int(amount) + int(balanceBefore)
            balance = Balance(balance_id=balance_id, amount_top_up=amount, balance_before=balanceBefore,
                              balance_after=balanceAfter, remarks='fail transaction')
            balance.user_id = user_id
            balance.save()


    channel.basic_consume(queue='payment_transfer', on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)