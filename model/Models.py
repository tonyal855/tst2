from db.Db import db
from datetime import datetime

class User(db.Document):
    user_id = db.StringField(required=True, unique=True)
    first_name = db.StringField(required=True)
    last_name = db.StringField(required=True)
    phone_number = db.IntField(required=True, unique=True)
    adress = db.StringField(required=True)
    pin = db.IntField(required=True)


class Balance(db.Document):
    balance_id = db.StringField(required=True, unique=True)
    seq = db.SequenceField()
    amount_top_up = db.IntField(required=True)
    balance_before = db.IntField(required=True)
    balance_after = db.IntField(required=True)
    remarks = db.StringField(required=False)
    created_date = db.DateTimeField(default=datetime.now())
    user_id = db.StringField(required=True)